<?php
/**
 * Created by PhpStorm.
 * User: Jerome Pinneau
 * Date: 01/10/2017
 * Time: 12:44
 */


//Menu
get_header();

//pop-up neos/techstudio
if (1>0) {
    if ($_SESSION['LANGUAGE'] == 'FR') {
        require_once(get_template_directory() . "/includes/indexFiles/homePopUpFR.php");
    } else if ($_SESSION['LANGUAGE'] == 'EN') {
        require_once(get_template_directory() . "/includes/indexFiles/homePopUpEN.php");
    }
}

//slide presentation
if ($_SESSION['LANGUAGE'] == 'FR'){
    echo do_shortcode("[contentblock id=homesliderfr]");
}else if ($_SESSION['LANGUAGE'] == 'EN' ){
    echo do_shortcode("[contentblock id=homeslideren]");
}
?>
<div class="main">
    <?php
    //Neos services
    if ($_SESSION['LANGUAGE'] == 'FR'){
    require_once(get_template_directory() . "/includes/indexFiles/homeServicesFR.php");
    }else if ($_SESSION['LANGUAGE'] == 'EN' ){
      require_once(get_template_directory() . "/includes/indexFiles/homeServicesEN.php");
    }
    ?>
    <hr class="divider-w">
    <!--<section class="module bg-dark-60" style='background-image: url("assets/images/section-3.jpg");' data-background="assets/images/section-3.jpg">
      <div class="container">
          <div class="row multi-columns-row">
              <div class="col-sm-6 col-md-3 col-lg-3">
                  <div class="count-item mb-sm-40">
                      <div class="count-icon"><span class="icon-wallet"></span></div>
                      <h3 class="count-to font-alt" data-countto="7">7</h3>
                      <h5 class="count-title font-serif">Created companies</h5>
                  </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                  <div class="count-item mb-sm-40">
                      <div class="count-icon"><span class="icon-profile-male"></span></div>
                      <h3 class="count-to font-alt" data-countto="25">25</h3>
                      <h5 class="count-title font-serif">Year of Experience</h5>
                  </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                  <div class="count-item mb-sm-40">
                      <div class="count-icon"><span class="icon-focus"></span></div>
                      <h3 class="count-to font-alt" data-countto="11">11</h3>
                      <h5 class="count-title font-serif">Companies Currently supported</h5>
                  </div>
              </div>
              <div class="col-sm-6 col-md-3 col-lg-3">
                  <div class="count-item mb-sm-40">
                      <div class="count-icon"><span class="icon-happy"></span></div>
                      <h3 class="count-to font-alt" data-countto="3">3</h3>
                      <h5 class="count-title font-serif">Success exits</h5>
                  </div>
              </div>
          </div>
      </div>
    </section>-->
    <?php
    //neos partenaires
    if ($_SESSION['LANGUAGE'] == 'FR'){
    require_once(get_template_directory() . "/includes/indexFiles/homePartenairesFR.php");
    }else if ($_SESSION['LANGUAGE'] == 'EN' ){
    require_once(get_template_directory() . "/includes/indexFiles/homePartenairesEN.php");
    }
    ?>

    <hr class="divider-w">

    <?php
    //neos team
    if ($_SESSION['LANGUAGE'] == 'FR'){
      require_once(get_template_directory() . "/includes/indexFiles/homeTeamFR.php");
    }else if ($_SESSION['LANGUAGE'] == 'EN' ){
      require_once(get_template_directory() . "/includes/indexFiles/homeTeamEN.php");
    }

    //testimonials
    if ($_SESSION['LANGUAGE'] == 'FR'){
      require_once(get_template_directory() . "/includes/indexFiles/homeTestimonialFR.php");
    }else if ($_SESSION['LANGUAGE'] == 'EN' ){
      require_once(get_template_directory() . "/includes/indexFiles/homeTestimonialEN.php");
    }

    //last articles
    if ($_SESSION['LANGUAGE'] == 'FR'){
      require_once(get_template_directory() . "/includes/indexFiles/homeLastArticlesFR.php");
    }else if ($_SESSION['LANGUAGE'] == 'EN' ){
      require_once(get_template_directory() . "/includes/indexFiles/homeLastArticlesEN.php");
    }

    //quote innovation and executiom
    if ($_SESSION['LANGUAGE'] == 'FR'){
      require_once(get_template_directory() . "/includes/indexFiles/homeInnovationFR.php");
    }else if ($_SESSION['LANGUAGE'] == 'EN' ){
      require_once(get_template_directory() . "/includes/indexFiles/homeInnovationEN.php");
    }

    //who are we
    if ($_SESSION['LANGUAGE'] == 'FR'){
      require_once(get_template_directory() . "/includes/indexFiles/homeWhoAreWeFR.php");
    }else if ($_SESSION['LANGUAGE'] == 'EN' ){
      require_once(get_template_directory() . "/includes/indexFiles/homeWhoAreWeEN.php");
    }
    ?>

    <hr class="divider-w">

    <?php
    //Form à traduire en francais
    if ($_SESSION['LANGUAGE'] == 'FR'){
      require_once(get_template_directory() . "/includes/indexFiles/homeInscriptionFR.php");
    }else if ($_SESSION['LANGUAGE'] == 'EN' ){
      require_once(get_template_directory() . "/includes/indexFiles/homeInscriptionEN.php");
    }

    //Form à traduire en anglais
    if ($_SESSION['LANGUAGE'] == 'FR'){
      require_once(get_template_directory() . "/includes/indexFiles/homeContactFR.php");
    }else if ($_SESSION['LANGUAGE'] == 'EN' ){
      require_once(get_template_directory() . "/includes/indexFiles/homeContactEN.php");
    }

    if ($_SESSION['LANGUAGE'] == 'FR'){
        get_footer();
    }else if ($_SESSION['LANGUAGE'] == 'EN' ){
        get_footer('EN');
    }
?>
