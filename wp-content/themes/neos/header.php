<?php
/*

    Neos Theme Header

*/
if(!isset($_SESSION['LANGUAGE']))
{
    $_SESSION['LANGUAGE'] = "EN";
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> dir="ltr">
  <head>

      <meta charset="<?php bloginfo( 'charset' ); ?>">
      <title>
		  <?php bloginfo('name'); ?>
		  <?php if (is_home() || is_front_page()) : ?>
			  <?php bloginfo('description'); ?>
		  <?php else : ?>
			  <?php wp_title("", true); ?>
		  <?php endif; ?>
      </title>
      <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
      <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>

      <meta charset="<?php bloginfo( 'charset' ); ?>">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="manifest" href="/manifest.json">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
      <meta name="theme-color" content="#ffffff">
	    <?php wp_head(); ?>
      <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

  </head>
  <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60" <?php body_class(); ?>>
    <main>
        <div class="page-loader" id="pageup">
            <div class="loader">Chargement...</div>
        </div>
        <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="<?php echo get_site_url(); ?>">
                        <img src="https://neos.lu/wp-content/uploads/2017/06/NEOS-logo-05.png" alt="Neos Luxembourg" id="toplogo"/>
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="custom-collapse">
                    <?php
                    if ($_SESSION['LANGUAGE'] == 'FR') {
                        require_once(get_template_directory() . "/includes/linkMenu/linkMenuFR.php");
                    } else if ($_SESSION['LANGUAGE'] == 'EN') {
                        require_once(get_template_directory() . "/includes/LinkMenu/linkMenuen.php");
                    }
                    ?>
                </div>

                <div class="choose-language">
                    <span class="sessionLanguage" data-language="FR" id="languageFR">FR</span> / <span class="sessionLanguage" data-language="EN" id="languageEN">EN</span>
                </div>
            </div>
        </nav>
