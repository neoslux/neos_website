<?php
/**
 * Created by PhpStorm.
 * User: Jerome Pinneau
 * Date: 20/10/2017
 * Time: 21:11
 */
?>
<div class="main">
	<section class="module">
		<div class="container">
			<div class="row post-masonry post-columns">
				<?php while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
					<div class="col-sm-6 col-md-4 col-lg-4">
						<div class="post">
							<div class="post-thumbnail"><a href="<?php the_permalink(); ?>">
									<?php if ( has_post_thumbnail() ) : ?>
										<?php the_post_thumbnail('thumbnail'); ?>
									<?php endif;?></a>
							</div>
							<div class="post-header font-alt">
								<h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
								<div class="post-meta">By&nbsp;<a href="<?php the_permalink(); ?>"><?php the_author() ?></a>&nbsp;| <?php the_time( 'j F' ); ?>
								</div>
							</div>
							<div class="post-more"><a class="more-link" href="<?php the_permalink(); ?>">Read more</a></div>
						</div>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	</section>
</div>
<?php wp_reset_postdata(); ?>
