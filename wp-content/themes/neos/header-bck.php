<?php
/*

    Neos Theme Header

*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> dir="ltr">
  <head>
      <meta charset="<?php bloginfo( 'charset' ); ?>">
      <title>
		  <?php bloginfo('name'); ?>
		  <?php if (is_home() || is_front_page()) : ?>
			  <?php bloginfo('description'); ?>
		  <?php else : ?>
			  <?php wp_title("", true); ?>
		  <?php endif; ?>
      </title>
      <meta charset="<?php bloginfo( 'charset' ); ?>">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="manifest" href="/manifest.json">
      <meta name="msapplication-TileColor" content="#ffffff">
      <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
      <meta name="theme-color" content="#ffffff">
	    <?php wp_head(); ?>
  </head>
  <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60" <?php body_class(); ?>>
    <main>
        <div class="page-loader" id="pageup">
            <div class="loader">Loading...</div>
        </div>
        <nav class="navbar navbar-custom navbar-fixed-top navbar-transparent" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="<?php echo get_site_url(); ?>">
                        <img src="https://neos.lu/wp-content/uploads/2017/06/NEOS-logo-05.png" alt="Neos Luxembourg" id="toplogo"/>
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="custom-collapse">
                    <ul class="nav navbar-nav navbar-right"> <!--  data-toggle="dropdown" -->
                            <li><a href="<?php if(!is_home()) { echo get_site_url(); } ?>#pageup"  class="section-scroll">Home</a></li>
                            <li><a href="<?php if(!is_home()) { echo get_site_url(); } ?>#services" class="section-scroll">Our services</a></li>
                            <li><a href="<?php if(!is_home()) { echo get_site_url(); } ?>#team" class="section-scroll">Our team</a></li>
                            <li><a href="<?php if(!is_home()) { echo get_site_url(); } ?>/Blog/" class="section-scroll">Blog</a></li>
                            <li><a href="<?php if(!is_home()) { echo get_site_url(); } ?>#about" class="section-scroll">About us</a>
                            <li><a href="<?php if(!is_home()) { echo get_site_url(); } ?>#contact" class="section-scroll">Contact us</a></li>
                    </ul>
                </div>
            </div>
        </nav>