<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

    <section id="primary" class=" module content-area" style="
    min-height: 97vh;">
        <main id="main" class="site-main">

            <?php
            the_content();

            ?>

        </main><!-- #main -->
    </section><!-- #primary -->

<?php
get_footer();
