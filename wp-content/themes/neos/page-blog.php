<?php
/**
		Template Name: Blog
 */

    get_header();

    $temp = $wp_query;
    $wp_query = new WP_Query();
    $wp_query->query('posts_per_page=11' . '&paged='.$paged);
?>

<?php get_template_part('blog_template'); ?>

<?php get_footer(); ?>