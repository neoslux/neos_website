<div id="login-pop" class="popup-con login-wrapper">
    <div class="popup-inner-con">
        <!--
                <a href="#" class="close-popup"><img src="{{ function('site_url') }}/wp-content/uploads/lesfrontaliers/close_round.svg"></a>
        -->
        <div class="subpart left-part">
            <div class="logo-wrapper">
                <img src="https://neos.lu/wp-content/uploads/2017/06/NEOS-logo-05.png" alt="Neos Luxembourg"/>
            </div>
            <div class="text-wrapper">
                <p> Société d'ingénierie informatique</p>
                <p>spécialisé dans</p>
                <p> l'innovation</p>
            </div>
            <div class="link-wrapper">
                <a class="close-link">Rester sur Neos.lu</a>
            </div>
        </div>
        <div class="subpart right-part">
            <div class="logo-wrapper">
                <img src="https://thetechstudio.co/wp-content/uploads/2018/12/logo-white.png" alt="The tech studio"/>
            </div>
            <div class="text-wrapper">
                <p>The Tech Angel, qui fournit un support technique complet du stade de développement du concept à la phase VC</p>
            </div>
            <div class="link-wrapper">
                <a target="_blank" href="https://thetechstudio.co/">Visiter thetechstudio.co</a>
            </div>
        </div>
    </div>

</div>