<section class="module" id="news">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt">Our last articles</h2>
                <!--<div class="module-subtitle font-serif">in our posts, we do not just present the life of the company, we also offer our advice, our opinions or some reactions of the team to various topics.</div>-->
            </div>
        </div>
        <div class="row multi-columns-row post-columns">
            <?php
            $temp = $wp_query;
            $wp_query = new WP_Query();
            $wp_query->query('posts_per_page=3' . '&paged=1');
            while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                <div class="col-sm-6 col-md-4 col-lg-4">
                    <div class="post mb-20">
                        <div class="post-thumbnail"><a href="<?php the_permalink(); ?>"><?php if ( has_post_thumbnail() ) : ?>
                                    <?php the_post_thumbnail('thumbnail'); ?>
                                <?php endif;?></a></div>
                        <div class="post-header font-alt">
                            <h2 class="post-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                            <div class="post-meta">By&nbsp;<a href="<?php the_permalink(); ?>"><?php the_author() ?></a>&nbsp;|&nbsp;<?php the_time( 'j F' ); ?>
                            </div>
                        </div>
                        <div class="post-more"><a class="more-link" href="<?php the_permalink(); ?>">Read more</a></div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <div style="width: 100%; text-align: center;"><input id="GoBlog" value="Show all articles" class="btn  btn-round btn-d" type="submit"></div>
        <?php wp_reset_postdata(); ?>
    </div>
</section>