<div class="module-small bg-dark">
    <div class="container">
        <div class="row" style="text-align: center;">
            <h2>Nos partenaires</h2>
            <div class="gs_logo_container col-12">
                <div class="gs_logo_single"><img src="https://neos.lu/wp-content/uploads/2017/10/Silicon_luxembourg.jpg" alt="Silicon Luxembourg Club" ></div>
                <div class="gs_logo_single"><img src="https://neos.lu/wp-content/uploads/2019/03/img.jpg" alt="Technoport" ></div>
                <div class="gs_logo_single"><img src="https://neos.lu/wp-content/uploads/2017/10/logo_GEN_H.png" alt="Grand Est Numérique" ></div>
                <div class="gs_logo_single"><img src="https://neos.lu/wp-content/uploads/2017/10/FF_Blue_vertical.png" alt="Frenchfounders" ></div>
                <div class="gs_logo_single"><img src="https://neos.lu/wp-content/uploads/2017/10/French-tech-horizontal1.png" alt="French tech" ></div>
            </div>
        </div>
    </div>
</div>