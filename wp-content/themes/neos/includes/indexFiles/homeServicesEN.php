<section class="module" id="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt">Our Services</h2>
                <div class="module-subtitle font-serif">
                    In the world of innovation, an idea alone is worthless. Only the technical concept and the execution of vision count.
                </div>
            </div>
        </div>
        <div class="row multi-columns-row">
            <div class="w-20 col-sm-6 col-xs-12">
                <div class="features-item">
                    <div class="features-icon"><img class="icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/big-data.png"></div>
                    <h3 class="features-title font-alt">Big Data</h3>
                </div>
            </div>
            <div class="w-20 col-sm-6 col-xs-12">
                <div class="features-item">
                    <div class="features-icon"><img class="icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/brain.png"></div>
                    <h3 class="features-title font-alt">Machine Learning</h3>
                    <!--                        <p>Careful attention to detail and clean, well structured code ensures a smooth user experience for all your visitors.</p>
                    -->                    </div>
            </div>
            <div class="w-20 col-sm-6 col-xs-12">
                <div class="features-item">
                    <div class="features-icon"><img class="icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/data.png"></div>
                    <h3 class="features-title font-alt">Web Scraping</h3>
                    <!--                        <p>Neos is above all stemmed from its men’s experience, 20 years in IT services, leading projects as various as complexes.</p>
                    -->                    </div>
            </div>
            <div class="w-20 col-sm-6 col-xs-12">
                <div class="features-item">
                    <div class="features-icon"> <img class="icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/mobile-app.png"></div>
                    <h3 class="features-title font-alt">Web & Mobile</h3>
                    <!--                        <p>We are able to quickly deliver and according to your initial needs thanks to our expertise and our experience.</p>
                    -->                    </div>
            </div>
            <div class="w-20 col-sm-6 col-xs-12">
                <div class="features-item">
                    <div class="features-icon"><img class="icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/chip.png"></div>
                    <h3 class="features-title font-alt">Artificial Intelligence</h3>
                    <!--                        <p>We are able to quickly deliver and according to your initial needs thanks to our expertise and our experience.</p>
                    -->                    </div>
            </div>
        </div>
        <div class="row" style="height: 20px;"></div>
        <div class="row">
            <div class="col-sm-12">
                <div role="tabpanel">
                    <ul class="nav nav-tabs font-alt" role="tablist">
                        <li class="active"><a href="#BigData" data-toggle="tab" aria-expanded="true">
                                <img class="small-icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/big-data.png">&nbsp;Big Data</a></li>
                        <li class=""><a href="#MachineLearning" data-toggle="tab" aria-expanded="false">
                                <img class="small-icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/brain.png">&nbsp;Machine learning</a></li>
                        <li class=""><a href="#Scraping" data-toggle="tab" aria-expanded="false">
                                <img class="small-icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/data.png">&nbsp;Scraping</a></li>
                        <li class=""><a href="#WebMobile" data-toggle="tab" aria-expanded="false">
                                <img class="small-icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/mobile-app.png">
                                Web & Mobile</a></li>
                        <li class=""><a href="#AI" data-toggle="tab" aria-expanded="false">
                                <img class="small-icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/chip.png">
                                Artificial Intelligence</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="BigData">
                            Digital data or data has upset companies in recent years and the way of thinking about information systems. Companies are collecting and storing data on all sectors but often clumsily. In a very large number of cases, these siled data are dormant and not highlighted.
                            NEOS offers you an innovative and effective approach to help you organize and manage your data to get the most out of it and solve the problems that are inerrant to Big Data (Volume, Speed, Variety (diversity), Veracity, Value).
                            For this, NEOS offers solutions based on free software to limit licensing costs, from transformation to modeling, from visualization to prediction.
                            <p>#BigData #MongoDB #Kibana Elastic search #MariaDB #Posgres #SQLExpress #Tableau #Talend #Data Cleaning #Data Mining #Data integration #Data collection #Data Analysis #Data Visualization </p>
                        </div>
                        <div class="tab-pane" id="MachineLearning">Machine learning has existed for many years but its exploitation was restricted to certain categories of companies because the necessary computing power was not available and many did not have enough data to train algorithms anyway. .
                            With the advent of big data, computing power is no longer a problem and there are massive amounts of data available on the internet to inflate the company's samples. It is no longer necessary to produce the data yourself and when the databases do not exist, it is quite easy to create them with a little mischief and scraping.
                            After a quick mapping of your data, Neos will be able to help you determine the processes and processes that will enable you to produce the most value in your data assets.
                            Generally articulated in 4 phases, Neos adapts to your needs and breaks down its:
                            1. Analyze your data assets
                            2. Defining the processes that will bring the most value to your business
                            3. Implement processes on your company's data and measure results
                            4. Collection and integration of new reference values ​​if necessary (growth of datasets via external games) and rerun of processes and measures
                            5. Implementation of processes and integration of these into your business processes (CRM, Sales, Production, After-sales, ...)
                            Neos accompanies you on the whole chain, from the study to the integration in your information system by always working with agility to allow you to measure at each stage the value acquired for your company.

                            <p>#supervised learning #semi-supervised learning #Unsupervised learning #Reinforcement learning #Anomaly detection #Association Rule Learning #Growth Hacking #Bot</p>
                        </div>
                        <div class="tab-pane" id="Scraping">

                            Scraping is sometimes misused and can cause "Negative SEO" if the goal is to copy internet content for use in your own content, and Neos will not do any type of service. However, it is very interesting to scrap content to build databases of references, repositories or to create samples of content.
                            Scraping aims to browse content (Web or electronic) to extract information to restore it in a structured format. Neos often offers this type of service to create sample databases to allow better learning algorithms or to create comparative databases (compare prices between different suppliers, resellers, ...). Still others use scraping to monitor competition and create alerts in case of policy / price / feature changes, ...
                            In short, scraping is now a common practice, but it can be expensive for non-specialists.
                            Thanks to its algorithms, Neos offers efficient and fast scraping solutions to implement and not only scrape web content, but also disk content or extract data from electronic documents or documents. scanned papers.

                            <p> #scraping #web #extract #images #OCR #text recognition #Pdf #Word #excel #translate #Crawler #Spider # Scraper #download #Character recognition</p>
                        </div>
                        <div class="tab-pane" id="WebMobile">

                            True vectors of innovation, we have forged an expertise in the deployment of native and multiplatform Web and Mobile applications (iOS & Android). We currently master a wide range of technologies on both the Web and Mobile side, enabling us to deploy optimized and adapted applications for diverse and varied business domains.

                        </div>
                        <div class="tab-pane" id="AI">

                            Many companies would like to use artificial intelligence. Certainly because of the false impression that artificial intelligence is out of reach, the majority of them do not take the leap. Only 15% of the companies have actually deployed technologies implementing artificial intelligence in 2018.
                            NEOS offers a step-by-step pragmatic approach that allows its customers to enter the IA area serenely. With a company-wide entry ticket, NEOS offers a personalized approach from data mapping and business processes, to the implementation of the first predictive algorithm, to the growth and consolidation of business processes. information. Indeed, many people think falsely that it takes a very large amount of data to start but it is not always necessary because on the one hand, it is possible to strengthen the data of the company artificially or to use sources to drive the algorithms, and secondly to use already efficient packages.
                            <p>#Regression #Instance-Based #Decision Tree #Clustering #Association Rule Learning #Ensemble #Natural language processing</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 100%; text-align: center;">
            <a href="<?php echo get_site_url(); ?>/application-form" id="Gobl" class="btn  btn-round btn-d" type="submit">Receive an estimate</a>
        </div>
        <div class="row">
            <div class="col-sm-2 col-sm-offset-5">
                <div class="large-text align-center"><a class="section-scroll" href="#team"><i class="fa fa-angle-down"></i></a></div>
            </div>
        </div>

    </div>
</section>