<section class="module" id="contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt">Contact</h2>
                <div class="module-subtitle font-serif"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <?php echo do_shortcode("[contact-form-7 title=\"Contact form 1\"]")?>
                <div class="ajax-response font-alt" id="contactFormResponse"></div>
            </div>
        </div>
    </div>
</section>