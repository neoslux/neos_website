<section class="module" id="about">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <h2 class="module-title font-alt">Qui sommes nous?</h2>
                <div class="module-subtitle font-serif large-text">Votre accélérateur technologique.

                    Passionnés de technologie et d’innovation, notre but est de vous accompagner dans vos projets technologiques en faisant tomber les barrières technologiques, vous permettant ainsi de vous concentrer sur le développement business.

                    Que vous soyez une startup désirant construire un MVP (Minimum Viable Product) rapidement, ou une société de taille plus conséquente ayant besoin d’accélérer dans la mise en oeuvre de vos projets technologiques, nous pouvons vous accompagner sur tout sujet, qu’il s’agisse d’applications Web/Mobile, ou de projets réputés plus “complexes”, comme l’Intelligence Artificielle, le Machine Learning, le Big Data, ou encore le Scraping.

                    Nous sommes des spécialistes dans l’execution de vos projets technologiques.

                    Vous avez une problématique sur laquelle vous souhaiteriez discuter ? Rendez-vous ici :</div>
                <div style="width: 100%; text-align: center;">
                    <a href="<?php echo get_site_url(); ?>/application-form" id="Gobl" class="btn  btn-round btn-d" type="submit">Obtenir une éstimation</a>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 col-sm-offset-5">
                <div class="large-text align-center"><a class="section-scroll" href="#contact"><i class="fa fa-angle-down"></i></a></div>
            </div>
        </div>
    </div>
</section>