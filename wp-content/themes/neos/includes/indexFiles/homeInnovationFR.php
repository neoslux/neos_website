<div class="module-small bg-dark">
    <div class="container">
        <div class="row" style="text-align: center;">
            <h2>Innovation et exécution!</h2>
            <h3>&laquo;&nbsp;Si j'avais demandé aux gens ce qu'ils voulaient, ils m'auraient répondu des chevaux plus rapides.&nbsp;&raquo; - Henry Ford</h3>
        </div>
    </div>
</div>