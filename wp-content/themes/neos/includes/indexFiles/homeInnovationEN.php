<div class="module-small bg-dark">
    <div class="container">
        <div class="row" style="text-align: center;">
            <h2>Innovation et execution!</h2>
            <h3>&laquo;&nbsp;If I had asked people what they wanted, they would have said faster horses.&nbsp;&raquo; - Henry Ford</h3>
        </div>
    </div>
</div>