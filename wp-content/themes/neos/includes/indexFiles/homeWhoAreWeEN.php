<section class="module" id="about">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <h2 class="module-title font-alt">Who are we?</h2>
                <div class="module-subtitle font-serif large-text">Your technological accelerator.

                    Passionate about technology and innovation, our goal is to support you in your technological projects by breaking down technological barriers, allowing you to focus on business development.

                    Whether you are a startup wishing to build a MVP (Minimum Viable Product) quickly, or a larger company that needs to accelerate the implementation of your technology projects, we can accompany you on any subject, it s It acts of Web / Mobile applications, or projects considered more "complex", like Artificial Intelligence, Machine Learning, Big Data, or Scraping.

                    We are specialists in the execution of your technological projects.

                    Do you have a problem you would like to discuss? Meet here :</div>
                <div style="width: 100%; text-align: center;">
                    <a href="<?php echo get_site_url(); ?>/application-form" id="Gobl" class="btn  btn-round btn-d" type="submit">Receive an estimate</a>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-2 col-sm-offset-5">
                <div class="large-text align-center"><a class="section-scroll" href="#contact"><i class="fa fa-angle-down"></i></a></div>
            </div>
        </div>
    </div>
</section>