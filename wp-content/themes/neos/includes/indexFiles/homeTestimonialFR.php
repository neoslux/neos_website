<section class="module bg-dark-60 pt-0 pb-0 parallax-bg testimonial" id="testimshort">
    <div class="testimonials-slider pt-140 pb-140">
        <ul class="slides">
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="module-icon"><span class="icon-quote">&nbsp;</span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <blockquote class="testimonial-text font-alt">Pour Klin, Neos n’aurait pas pu arriver à un meilleur moment. En effet, nous avons dû revoir from scratch toute notre plateforme web, pour un résultat très performant. De plus, les services de Neos sont tailor-mades pour toute Startup et PME, je ne peux donc que les recommander.  Dernier point très important : La team est vraiment sympa !</blockquote>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="testimonial-author">
                                <div class="testimonial-caption font-alt">
                                    <div class="testimonial-title">Antoine H.</div>
                                    <div class="testimonial-descr">Partner Klin.lu</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="module-icon"><span class="icon-quote"></span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <blockquote class="testimonial-text font-alt">Neos a été capable de nous développer un outil de tracking comportemental qui m’a vraiment aidé dans le développement de mon business. Un grand merci à vous l’équipe, pour le travail accompli et le temps que vous m’avez fait gagner.</blockquote>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="testimonial-author">
                                <div class="testimonial-caption font-alt">
                                    <div class="testimonial-title">Thierry R.</div>
                                    <div class="testimonial-descr">General Manager</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="module-icon"><span class="icon-quote"></span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <blockquote class="testimonial-text font-alt">Nous travaillons avec l’équipe de Neos depuis très longtemps maintenant (avant même la création de la structure), et nous avons toujours été satisfaits. Nous pouvons nous concentrer sur le business et notre expertise tout en laissant l’équipe s’occuper de la composante technologique.</blockquote>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="testimonial-author">
                                <div class="testimonial-caption font-alt">
                                    <div class="testimonial-title">Jean-Louis COËDEL</div>
                                    <div class="testimonial-descr">Manager at Stevimmac</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>