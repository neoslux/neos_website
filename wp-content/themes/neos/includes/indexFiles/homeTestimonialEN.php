<section class="module bg-dark-60 pt-0 pb-0 parallax-bg testimonial" id="testimshort">
    <div class="testimonials-slider pt-140 pb-140">
        <ul class="slides">
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="module-icon"><span class="icon-quote">&nbsp;</span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <blockquote class="testimonial-text font-alt">
                                For Klin, Neos could not have come at a better time. Indeed, we had to review from scratch all our web platform, for a very powerful result. In addition, Neos services are tailor-mades for any Startup and SME, so I can only recommend them. Last, very important point: The team is really nice!</blockquote>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="testimonial-author">
                                <div class="testimonial-caption font-alt">
                                    <div class="testimonial-title">Antoine H.</div>
                                    <div class="testimonial-descr">Partner Klin.lu</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="module-icon"><span class="icon-quote"></span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <blockquote class="testimonial-text font-alt">
                                Neos was able to develop a behavioral tracking tool that really helped me in the development of my business. A big thank you to the team, for the work done and the time you made me win.</blockquote>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="testimonial-author">
                                <div class="testimonial-caption font-alt">
                                    <div class="testimonial-title">Thierry R.</div>
                                    <div class="testimonial-descr">General Manager</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="module-icon"><span class="icon-quote"></span></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <blockquote class="testimonial-text font-alt">
                                We have been working with the Neos team for a very long time now (even before the creation of the structure), and we have always been satisfied. We can focus on the business and our expertise while letting the team take care of the technology component.</blockquote>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-4">
                            <div class="testimonial-author">
                                <div class="testimonial-caption font-alt">
                                    <div class="testimonial-title">Jean-Louis COËDEL</div>
                                    <div class="testimonial-descr">Manager at Stevimmac</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</section>