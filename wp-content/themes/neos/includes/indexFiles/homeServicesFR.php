<section class="module" id="services">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt">Nos Services</h2>
                <div class="module-subtitle font-serif">Dans le monde de l’innovation, une idée, seule, n’a pas de valeur. Seule le concept technique et l’execution de la vision comptent.</div>
            </div>
        </div>
        <div class="row multi-columns-row">
            <div class="w-20 col-sm-6 col-xs-12">
                <div class="features-item">
                    <div class="features-icon"><img class="icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/big-data.png"></div>
                    <h3 class="features-title font-alt">Big Data</h3>
                </div>
            </div>
            <div class="w-20 col-sm-6 col-xs-12">
                <div class="features-item">
                    <div class="features-icon"><img class="icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/brain.png"></div>
                    <h3 class="features-title font-alt">Machine Learning</h3>
                    <!--                        <p>Careful attention to detail and clean, well structured code ensures a smooth user experience for all your visitors.</p>
                    -->                    </div>
            </div>
            <div class="w-20 col-sm-6 col-xs-12">
                <div class="features-item">
                    <div class="features-icon"><img class="icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/data.png"></div>
                    <h3 class="features-title font-alt">Web Scraping</h3>
                    <!--                        <p>Neos is above all stemmed from its men’s experience, 20 years in IT services, leading projects as various as complexes.</p>
                    -->                    </div>
            </div>
            <div class="w-20 col-sm-6 col-xs-12">
                <div class="features-item">
                    <div class="features-icon"> <img class="icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/mobile-app.png"></div>
                    <h3 class="features-title font-alt">Web & Mobile</h3>
                    <!--                        <p>We are able to quickly deliver and according to your initial needs thanks to our expertise and our experience.</p>
                    -->                    </div>
            </div>
            <div class="w-20 col-sm-6 col-xs-12">
                <div class="features-item">
                    <div class="features-icon"><img class="icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/chip.png"></div>
                    <h3 class="features-title font-alt">Artificial Intelligence</h3>
                    <!--                        <p>We are able to quickly deliver and according to your initial needs thanks to our expertise and our experience.</p>
                    -->                    </div>
            </div>
        </div>
        <div class="row" style="height: 20px;"></div>
        <div class="row">
            <div class="col-sm-12">
                <div role="tabpanel">
                    <ul class="nav nav-tabs font-alt" role="tablist">
                        <li class="active"><a href="#BigData" data-toggle="tab" aria-expanded="true">
                                <img class="small-icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/big-data.png">&nbsp;Big Data</a></li>
                        <li class=""><a href="#MachineLearning" data-toggle="tab" aria-expanded="false">
                                <img class="small-icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/brain.png">&nbsp;Machine learning</a></li>
                        <li class=""><a href="#Scraping" data-toggle="tab" aria-expanded="false">
                                <img class="small-icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/data.png">&nbsp;Scraping</a></li>
                        <li class=""><a href="#WebMobile" data-toggle="tab" aria-expanded="false">
                                <img class="small-icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/mobile-app.png">
                                Web & Mobile</a></li>
                        <li class=""><a href="#AI" data-toggle="tab" aria-expanded="false">
                                <img class="small-icon-feature" src="<?php echo get_site_url()?>/wp-content/uploads/icons/chip.png">
                                Artificial Intelligence</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="BigData">Les données numériques ou data ont bouleversé les entreprises ces dernières années et la façon de penser les systèmes d’information. Les entreprises se mettent à collecter et à stocker des données sur tous les secteurs mais souvent de façon maladroite. Dans un très grand nombre de cas, ces données cloisonnées dorment et ne sont pas mise en valeur.
                            NEOS vous propose une approche innovante et efficace pour vous aider à organiser et gérer vos données pour en tirer le meilleur parti et résoudre les problèmes inerrants au Big data (Volume, Vitesse, Variété (diversité), Véracité, Valeur).
                            Pour cela, NEOS propose des solutions basées sur des logiciels libres pour limiter les coûts en licences, de la transformation à la modélisation, de la visualisation a la prédiction
                            <p>#BigData #MongoDB #Kibana Elastic search #MariaDB #Posgres #SQLExpress #Tableau #Talend #Data Cleaning #Data Mining #Data integration #Data collection #Data Analysis #Data Visualization </p>
                        </div>
                        <div class="tab-pane" id="MachineLearning">Le machine learning existe depuis de nombreuses années mais son exploitation n’était réservée qu’a certaines catégories d’entreprises car la puissance de calcul nécessaire n’était pas disponible et beaucoup n’avaient de toute façon pas assez de données pour entrainer des algorithmes.
                            Avec l’avènement du big data, la puissance de calcul n’est plus vraiment un problème et il existe des masses de données impressionnantes disponibles sur internet pour gonfler les échantillons de l’entreprise. Il n’est plus nécessaire de produire soi-même les données et quand les bases de données n’existent pas, il est assez facile de les créer avec un peu de malice et de scraping.
                            Après une cartographie rapide de vos données, Neos sera en mesure de vous aider à déterminer les processus et procédés qui vous permettront de produire le plus de valeur de votre patrimoine de données.
                            Généralement articulé en 4 phases, Neos s’adapte à vos besoins et décompose ses :
                            1.    Analyse de votre patrimoine de données
                            2.    Définition des processus qui apporteront le plus de valeur à votre entreprise
                            3.    Mise en œuvre des processus sur les données de votre entreprise et mesure des résultats
                            4.    Collecte et intégration de nouvelles valeurs de références si besoin (croissance des jeux de données via des jeux externes) et réexécution des processus et mesures
                            5.    Mise en œuvre des procédés et intégration de ceux-ci dans vos processus d’entreprises (CRM, Vente, Production, Après-vente,…)
                            Neos vous accompagne sur toute la chaine, de l’étude à l’intégration dans votre système d’information en travaillant toujours avec agilité pour vous permettre de mesurer à chaque étape la valeur acquise pour votre entreprise.

                            <p>#supervised learning #semi-supervised learning #Unsupervised learning #Reinforcement learning #Anomaly detection #Association Rule Learning #Growth Hacking #Bot</p>
                        </div>
                        <div class="tab-pane" id="Scraping">
                            Le scraping est quelque fois utilisé à mauvais escient et peut provoquer du « Negative SEO » si l’objectif est de recopier des contenus internet pour les utiliser dans vos propres contenus, et Neos ne fera pas de type de prestation. Il est toutefois très intéressant de scraper des contenus pour constituer des bases de données de références, des référentiels ou encore pour créer des échantillons de contenus.
                            Le scraping vise à parcourir des contenus ( Web ou électronique ) afin d’extraire des informations pour les restituer dans un format structuré. Neos propose souvent ce type de prestation pour créer des bases de données d’échantillon afin de permettre un meilleur apprentissage des algorithmes ou encore pour créer des bases de données comparatives (comparer les prix entre différents fournisseurs, revendeurs, …). D’autres encore utilisent le scraping pour surveiller la concurrence et créer des alertes en cas de changement de politique / de prix / de caractéristiques, …
                            Bref, le scraping est aujourd’hui une pratique courante, mais celle-ci peut être couteuse pour des non-spécialiste.
                            Grâce à ses algorithmes, Neos propose des solutions de scraping efficaces et rapides à mettre en œuvre et permettent non seulement de scraper des contenus web, mais aussi des contenus sur disque ou encore de l’extraction de données à partir de documents électroniques ou de documents papiers scannés.

                            <p> #scraping #web #extract #images #OCR #text recognition #Pdf #Word #excel #translate #Crawler #Spider # Scraper #download #Character recognition</p>
                        </div>
                        <div class="tab-pane" id="WebMobile">
                            Véritables vecteurs d’innovation, nous nous sommes forgés une expertise dans le déploiement d’applications Web et Mobile natives et multiplateformes (iOS & Android). Nous maîtrisons aujourd’hui une large gamme de technologies aussi bien sur la partie Web que Mobile, nous permettant de pouvoir déployer des applications optimisées et adaptées pour des domaines business divers et variés.

                        </div>
                        <div class="tab-pane" id="AI">
                            Beaucoup d’entreprises aimeraient utiliser de l’intelligence artificielle. Certainement par méconnaissance par la fausse impression que l’intelligence artificielle est hors de portée, la majorité d’entre elles ne sautent pas le pas. Seulement 15% des entreprises ont réellement déployé des technologies mettant en œuvre de l’intelligence artificielle en 2018.
                            NEOS propose une approche pragmatique « étape par étape » permettant à ses clients d’entrer dans l’aire « IA » sereinement. Avec un ticket d’entrée approprié à la taille de l’entreprise, NEOS propose une approche personnalisée allant de la cartographie des données et des processus métiers, à la mise en œuvre des premiers algorithme prédictifs, en passant par la croissance et la consolidation des informations. En effet, beaucoup pensent faussement qu’il faut une quantité très importante de données pour débuter mais ce n’est pas toujours nécessaire car d’une part, il est possible de renforcer les données de l’entreprise artificiellement ou d’utiliser des sources externes pour entrainer les algorithmes, et d’autre part, d’utiliser des packages déjà efficace.
                            <p>#Regression #Instance-Based #Decision Tree #Clustering #Association Rule Learning #Ensemble #Natural language processing</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div style="width: 100%; text-align: center;">
            <a href="<?php echo get_site_url(); ?>/application-form" id="Gobl" class="btn  btn-round btn-d" type="submit">Obtenir une éstimation</a>
        </div>
        <div class="row">
            <div class="col-sm-2 col-sm-offset-5">
                <div class="large-text align-center"><a class="section-scroll" href="#team"><i class="fa fa-angle-down"></i></a></div>
            </div>
        </div>

    </div>
</section>