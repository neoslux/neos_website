<div class="module-small bg-dark" id="Subscribe">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-4 col-lg-offset-2">
                <div class="callout-text font-alt">
                    <h3 class="callout-title">Register</h3>
                    <p>We will not spam your email.</p>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <div class="callout-btn-box">
                    <?php echo do_shortcode("[mc4wp_form id=\"Newsletter\"]") ?>
                    <div class="text-center" id="subscription-response"></div>
                </div>
            </div>
        </div>
    </div>
</div>