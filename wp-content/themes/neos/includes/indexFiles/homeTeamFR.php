<section class="module" id="team">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <h2 class="module-title font-alt">Notre équipe</h2>
                <div class="module-subtitle font-serif">Une équipe de passionnés de Technologie et d’innovation pour mener vos projets à bien.</div>
            </div>
        </div>
        <div class="row team-slider">
            <!--<div class="mb-sm-20 wow fadeInUp col-sm-6 col-md-3" onclick="wow fadeInUp">
              <div class="team-item">
                <div class="team-image"><img src="https://neos.lu/wp-content/uploads/2017/10/jerome-pinneau.png" alt="Member Photo"/>
                  <div class="team-detail">
                      <h5 class="font-alt"></h5>
                      <p class="font-serif"></p>
                    <div class="team-social"><a href="https://twitter.com/Jerome_pinneau" target="_blank"><i class="fa fa-twitter"></i></a><a href="https://www.linkedin.com/in/jeromepinneau/" target="_blank"><i class="fa fa-linkedin"></i></a></div>
                  </div>
                </div>
                <div class="team-descr font-alt">
                  <div class="team-name">Jérôme Pinneau</div>
                  <div class="team-role">Owner</div>
                </div>
              </div>
            </div>-->
            <div class="mb-sm-20 wow fadeInUp col-sm-6 col-md-3" onclick="wow fadeInUp">
                <div class="team-item">
                    <div class="team-image"><img src="https://neos.lu/wp-content/uploads/2017/10/David.jpg" alt="Member Photo"/>
                        <div class="team-detail">
                            <h5 class="font-alt"></h5>
                            <p class="font-serif"></p>
                            <div class="team-social"><a href="https://www.linkedin.com/in/david-pinneau/" target="_blank"><i class="fa fa-linkedin"></i></a></div>
                        </div>
                    </div>
                    <div class="team-descr font-alt">
                        <div class="team-name">David Pinneau</div>
                        <div class="team-role">Co-founder & Managing Director</div>
                    </div>
                </div>
            </div>
            <div class="mb-sm-20 wow fadeInUp col-sm-6 col-md-3" onclick="wow fadeInUp">
                <div class="team-item">
                    <div class="team-image"><img src="https://neos.lu/wp-content/uploads/2018/06/img.jpg" alt="Member Photo"/>
                        <div class="team-detail">
                            <h5 class="font-alt"></h5>
                            <p class="font-serif"></p>
                            <div class="team-social"><a href="https://www.linkedin.com/in/arthurscheiber/" target="_blank"><i class="fa fa-linkedin"></i></a></div>
                        </div>
                    </div>
                    <div class="team-descr font-alt">
                        <div class="team-name">Arthur Scheiber</div>
                        <div class="team-role">Technical Leader</div>
                    </div>
                </div>
            </div>

            <div class="mb-sm-20 wow fadeInUp col-sm-6 col-md-3" onclick="wow fadeInUp">
                <div class="team-item">
                    <div class="team-image"><img src="https://neos.lu/wp-content/uploads/2018/05/Jonathan.jpg" alt="Member Photo"/>
                        <div class="team-detail">
                            <h5 class="font-alt"></h5>
                            <p class="font-serif"></p>
                            <div class="team-social"><a href="https://www.linkedin.com/in/jonathan-lopez-donato/" target="_blank"><i class="fa fa-linkedin"></i></a></div>
                        </div>
                    </div>
                    <div class="team-descr font-alt">
                        <div class="team-name">Jonathan Lopez-Donato</div>
                        <div class="team-role">Full Stack Developer & Project Leader</div>
                    </div>
                </div>
            </div>
            <div class="mb-sm-20 wow fadeInUp col-sm-6 col-md-3" onclick="wow fadeInUp">
                <div class="team-item">
                    <div class="team-image"><img src="https://neos.lu/wp-content/uploads/2018/05/Alex.jpg" alt="Member Photo"/>
                        <div class="team-detail">
                            <h5 class="font-alt"></h5>
                            <p class="font-serif"></p>
                            <div class="team-social"></div>
                        </div>
                    </div>
                    <div class="team-descr font-alt">
                        <div class="team-name">Alex Queval</div>
                        <div class="team-role">Full-stack developer</div>
                    </div>
                </div>
            </div>
            <div class="mb-sm-20 wow fadeInUp col-sm-6 col-md-3" onclick="wow fadeInUp">
                <div class="team-item">
                    <div class="team-image"><img src="https://neos.lu/wp-content/uploads/2017/10/Julien.jpg" alt="Member Photo"/>
                        <div class="team-detail">
                            <h5 class="font-alt"></h5>
                            <p class="font-serif"></p>
                            <div class="team-social"><a href="https://www.linkedin.com/in/julien-barthelemy-944664138/" target="_blank"><i class="fa fa-linkedin"></i></a></div>
                        </div>
                    </div>
                    <div class="team-descr font-alt">
                        <div class="team-name">Julien Barthelemy</div>
                        <div class="team-role">Designer Web and Print</div>
                    </div>
                </div>
            </div>
            <div class="mb-sm-20 wow fadeInUp col-sm-6 col-md-3" onclick="wow fadeInUp">
                <div class="team-item">
                    <div class="team-image"><img src="https://neos.lu/wp-content/uploads/2017/10/Nicolas.jpg" alt="Member Photo"/>
                        <div class="team-detail">
                            <h5 class="font-alt"></h5>
                            <p class="font-serif"></p>
                            <div class="team-social"></div>
                        </div>
                    </div>
                    <div class="team-descr font-alt">
                        <div class="team-name">Nicolas Schaeffer</div>
                        <div class="team-role">Developer</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="row">
        <div class="col-sm-2 col-sm-offset-5">
            <div class="large-text align-center"><a class="section-scroll" href="#testimshort"><i class="fa fa-angle-down"></i></a></div>
        </div>
    </div>
</section>