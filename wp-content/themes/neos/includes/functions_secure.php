<?php


/* Cache la version de wordpress */
/* desactive l'éditeur de fichier en ligne */
/* desactivation du xml-dpc */
add_filter('xmlrpc_enabled', '__return_false');

remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );
remove_action('wp_head', 'rsd_link'); //removes EditURI/RSD (Really Simple Discovery) link.
remove_action('wp_head', 'wlwmanifest_link'); //removes wlwmanifest (Windows Live Writer) link.
remove_action('wp_head', 'wp_generator'); //removes meta name generator.
remove_action('wp_head', 'wp_shortlink_wp_head'); //removes shortlink.
remove_action( 'wp_head', 'feed_links', 2 ); //removes feed links.
remove_action('wp_head', 'feed_links_extra', 3 );  //removes comments feed.

//Wp admin bar non visible pour les abonnés
if (!current_user_can('edit_posts')) {
}
