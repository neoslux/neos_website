<ul class="nav navbar-nav navbar-right"><!-- data-toggle="dropdown" -->
    <li><a href="<?php if(!is_home()) { echo get_site_url(); } ?>#services" class="section-scroll">Our services</a></li>
    <li><a href="<?php if(!is_home()) { echo get_site_url(); } ?>#about" class="section-scroll">About us</a></li>
    <li><a href="<?php if(!is_home()) { echo get_site_url(); } ?>#contact" class="section-scroll">Contact us</a></li>
    <li><a class="estimate-navbar" href="<?php echo get_site_url()?>/application-form" style="font-weight: bold;">Receive an estimate</a></li>
</ul>