<?php
/**
 * Created by PhpStorm.
 * User: Jerome Pinneau
 * Date: 23/09/2017
 * Time: 12:56
 */

class Digix{
    public $_prenom;
    public $_nom;
    public $_complet;
}

//error_log( "//" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);

/* Cache la version de wordpress */
remove_action("wp_head", "wp_generator");
/* desactive l'éditeur de fichier en ligne */
define('DISALLOW_FILE_EDIT',true);
/* desactivation du xml-dpc */
add_filter('xmlrpc_enabled', '__return_false');

require_once( get_template_directory() . '/includes/functions_secure.php' );
add_theme_support( 'post-thumbnails' );
add_filter( 'pre_comment_content' , 'wp_strip_all_tags' );

add_action( 'wp_enqueue_scripts', 'pageScripts' );
function pageScripts() {

    if (  is_page( 'asana' )) {
        error_log('poetpoet');
    }

	wp_enqueue_style( 'bootstrapmin', get_template_directory_uri() . '/assets/lib/bootstrap/dist/css/bootstrap.min.css' );
	wp_enqueue_style( 'googleapi', 'https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700' );
	wp_enqueue_style( 'googleapi2', 'https://fonts.googleapis.com/css?family=Volkhov:400i' );
	wp_enqueue_style( 'googleapi3', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' );
	wp_enqueue_style( 'Stylesheet1', get_template_directory_uri() . '/assets/lib/animate.css/animate.css' );
	wp_enqueue_style( 'Stylesheet2', get_template_directory_uri() . '/assets/lib/components-font-awesome/css/font-awesome.min.css' );
	wp_enqueue_style( 'Stylesheet3', get_template_directory_uri() . '/assets/lib/et-line-font/et-line-font.css' );
	wp_enqueue_style( 'Stylesheet4', get_template_directory_uri() . '/assets/lib/flexslider/flexslider.css' );
	wp_enqueue_style( 'Stylesheet5', get_template_directory_uri() . '/assets/lib/owl.carousel/dist/assets/owl.carousel.min.css' );
	wp_enqueue_style( 'Stylesheet6', get_template_directory_uri() . '/assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css' );
	wp_enqueue_style( 'Stylesheet7', get_template_directory_uri() . '/assets/lib/magnific-popup/dist/magnific-popup.css' );
	wp_enqueue_style( 'Stylesheet8', get_template_directory_uri() . '/assets/lib/simple-text-rotator/simpletextrotator.css' );
	wp_enqueue_style( 'Stylesheet9', get_template_directory_uri() . '/assets/css/style.css?v=4' );
	wp_enqueue_style( 'Stylesheet10', get_template_directory_uri() . '/assets/css/colors/default.css' );
    wp_enqueue_style( 'Stylesheet11', get_template_directory_uri() . '/assets/css/popup.css' );

	wp_enqueue_script( "jquery1", get_template_directory_uri() . '/assets/lib/jquery/dist/jquery.js',  array( 'jquery' ), '', true );
	wp_enqueue_script( "jquery2", get_template_directory_uri() . '/assets/lib/bootstrap/dist/js/bootstrap.min.js',  array( 'jquery' ), '', true );
	wp_enqueue_script( "script3", get_template_directory_uri() . '/assets/lib/wow/dist/wow.js',  array( 'jquery' ), '', true );
	wp_enqueue_script( "script4", get_template_directory_uri() . '/assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js',  array( 'jquery' ), '', true );
	wp_enqueue_script( "script5", get_template_directory_uri() . '/assets/lib/isotope/dist/isotope.pkgd.js',  array( 'jquery' ), '', true );
	wp_enqueue_script( "script6", get_template_directory_uri() . '/assets/lib/imagesloaded/imagesloaded.pkgd.js',  array( 'jquery' ), '', true );
	wp_enqueue_script( "script7", get_template_directory_uri() . '/assets/lib/flexslider/jquery.flexslider.js',  array( 'jquery' ), '', true );
	wp_enqueue_script( "script8", get_template_directory_uri() . '/assets/lib/owl.carousel/dist/owl.carousel.min.js',  array( 'jquery' ), '', true );
	wp_enqueue_script( "script9", get_template_directory_uri() . '/assets/lib/smoothscroll.js',  array( 'jquery' ), '', true );
	wp_enqueue_script( "script10", get_template_directory_uri() . '/assets/lib/magnific-popup/dist/jquery.magnific-popup.js',  array( 'jquery' ), '', true );
	wp_enqueue_script( "script11", get_template_directory_uri() . '/assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js',  array( 'jquery' ),
        '', true );
	wp_enqueue_script( "script12", get_template_directory_uri() . '/assets/js/plugins.js',  array( 'jquery' ), '', true );
	wp_enqueue_script( "script13", get_template_directory_uri() . '/assets/js/main.js',  array( 'jquery' ), '', true );
    wp_localize_script( 'script13', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
	wp_enqueue_script( "script14", get_template_directory_uri() . '/assets/lib/slick/slick.js',  array( 'jquery' ), '', true );
}

add_action('wp_ajax_TestDigix','TestDigixData');
add_action('wp_ajax_nopriv_TestDigix','TestDigixData');
function TestDigixData(){
    try{
        if(isset($_POST['prenom']) && isset($_POST['nom']))
        {
            $Dd = new Digix();
            $Dd->_complet = $_POST['prenom'] . " " . $_POST['nom'];
            $Dd->_prenom = $_POST['prenom'];
            $Dd->_nom = $_POST['nom'];
            wp_die(wp_json_encode($Dd));
        }
    }catch (Exception $e){
        echo($e->getMessage());
    }
    die();
}

/* Regionalisation */

add_action( 'wp_ajax_ChooseLanguage', 'ChooseLanguage' );

add_action( 'wp_ajax_nopriv_ChooseLanguage', 'ChooseLanguage' );

function ChooseLanguage() {

    $_SESSION['LANGUAGE'] = $_POST['LANGUAGE'];
    wp_send_json($_POST['LANGUAGE']);

}

