<?php

/**
 * The template for displaying all single posts and attachments
 *
 * @package Neos
 *
 */

get_header();

$categories = get_categories(array(
	'orderby' => 'name',
	'order'   => 'ASC'
));
$tags = get_tags(array(
	'orderby' => 'name',
	'order'   => 'ASC'
));
?>
<div class="main">
	<section class="module-small" id="post-<?php the_ID(); ?>">
		<div class="container">
			<div class="row">
				<div class="col-sm-8">
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="post">
						<div class="post-thumbnail">
							<?php if ( has_post_thumbnail() ) {
								the_post_thumbnail();
							}  ?>
						</div>
						<div class="post-header font-alt">
							<h1 class="post-title"><?php the_title(); ?></h1>
							<div class="post-meta">By&nbsp;<a href="#"><?php the_author(); ?></a>| <?php echo get_the_date(); ?> | <?php comments_popup_link('No Comments »', '1 Comment »', '% Comments »'); ?> | Posted in <?php the_category(', ') ?><?php edit_post_link('Edit',' | ',''); ?>
							</div>
						</div>
						<div class="post-entry">
							<?php the_content(); ?>
						</div>
					</div>
					<div class="comments" id="comments">
						<h4 class="comment-title font-alt">There are <?php comments_number('no comments »', '1 Comment »', '% Comments »'); ?></h4>
						<?php
							$comments = get_comments( array( 'post_id' => get_the_ID() ) );
							foreach ( $comments as $comment ) :
                                if($comment->comment_approved == 1){
						?>
						<div class="comment clearfix">
							<div class="comment-avatar"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/wordpress-6-xxl.png" alt="avatar"/></div>
							<div class="comment-content clearfix">
								<div class="comment-author font-alt"><a href="<? $comment->comment_author_url ?>"><?php echo $comment->comment_author; ?></a></div>
								<div class="comment-body">
									<p><?php echo $comment->comment_content ?></p>
								</div>
								<div class="comment-meta font-alt"><?php echo $comment->comment_date; ?><!-- - <a href="">Reply</a>-->
								</div>
							</div>
						</div>
						<?php } endforeach; ?>
					</div>
					<div class="comment-form" class="hidden">
						<!--<h4 class="comment-form-title font-alt">Add your comment</h4>-->
						<?php
						/*$args = array(
							'comment_field' => '<p class="comment-form-comment"><label class="sr-only" for="comment">' . _x( 'Comment', 'noun' ) . '</label><textarea placeholder="Comment" class="form-control" id="comment" name="comment" cols="45" rows="8" aria-required="true" required></textarea></p>',
							'fields' => apply_filters( 'comment_form_default_fields', array(

								'author' =>
									'<div class="form-group">' .
									'<label class="sr-only" for="author">' . __( 'Name', 'domainreference' ) . '</label> ' .
									( $req ? '<span class="required">*</span>' : '' ) .
									'<input id="author" name="author" type="text" class="form-control" placeholder="Name" value="' . esc_attr( $commenter['comment_author'] ) .
									'" size="30"' . $aria_req . ' required /></div>',

								'email' =>
									'<div class="form-group"><label class="sr-only" for="email">' . __( 'Email', 'domainreference' ) . '</label> ' .
									( $req ? '<span class="required">*</span>' : '' ) .
									'<input id="email" name="email" type="text"  class="form-control" placeholder="E-mail" value="' . esc_attr(  $commenter['comment_author_email'] ) .
									'" size="30"' . $aria_req . ' required /></div>',

								'url' =>
									'<div class="form-group"><label class="sr-only"  for="url">' .
									__( 'Website', 'domainreference' ) . '</label>' .
									'<input id="url" name="url" type="text"  class="form-control" placeholder="Website" value="' . esc_attr( $commenter['comment_author_url'] ) .
									'" size="30" /></div>'
							)

						));
							comment_form( $args );*/
						?>
					</div>
					<?php endwhile; else :
						esc_html_e( 'Sorry, no posts matched your criteria.' );
					endif; ?>
				</div>
				<div class="col-sm-4 col-md-3 col-md-offset-1 sidebar">
					<div class="widget">
						<form role="form">
							<div class="search-box">
								<input class="form-control" type="text" placeholder="Search..."/>
								<button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
							</div>
						</form>
					</div>
					<div class="widget">
						<h5 class="widget-title font-alt">Blog Categories</h5>
						<ul class="icon-list"><?php
                            $count_categories = count($categories);
                            foreach( $categories as $category ) {
                            $link = get_category_link( $category->term_id );
                            $title = $category->name;
                            echo '<li><a href="'.$link.'">'.$title.'</a></li>';
                            } ?>
						</ul>
					</div>
					<div class="widget">
						<h5 class="widget-title font-alt">Blog Tag</h5>
                        <div class="tags font-serif">
							<?php
							foreach( $tags as $tag ) {
								$link = get_category_link( $tag->term_id );
								$title = $tag->name;
								echo '<a href="'.$link.'" rel="tag">#'.$title.'</a>';
							} ?>
                        </div>
                    </div>
					<div class="widget">
						<h5 class="widget-title font-alt">Recent Comments</h5>
						<ul class="icon-list">
                            <?php
                                $args = array();
                                $comments_query = new WP_Comment_Query;
                                $comments = $comments_query->query($args);
                                if ($comments) {
                                    foreach ($comments as $comment) {
                                        if($comment->status=="approve")
                                        {
                                            $id      = $comment->comment_ID;
                                            $author  = $comment->comment_author;
                                            $comment = $comment->comment_content;
                                            $date    = get_comment_date('l, F jS, Y', $id);
                                            $url     = get_comment_link($id);
                                            echo  '<li>'. $author .'<a href="'. $url .'" title="'. $date .'">' . wp_strip_all_tags($comment, true) . '</a></li>';
                                        }
                                    }
                                } else {
                                    echo __('No recent comments.', 'shapespace');
                                }
                            ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section>
<div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
<?php get_footer(); ?>
