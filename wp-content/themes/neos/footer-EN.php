<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package Neos
 *
 */
?>
<hr class="divider-w">
<div class="module-small bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="widget">
                    <h5 class="widget-title font-alt">About Neos Luxembourg</h5>
                    <p>Technoport , Bâtiment admin <br/>  Rue du Commerce<br/> 3895 - Foetz – Luxembourg</p>
                    <p>Phone: +352 54 55 80 782</p>
                    <p>Email:&nbsp;&nbsp;<a href="#">contact@neos.lu</a></p>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="widget">
                    <h5 class="widget-title font-alt">Blog</h5><?php

                    $categories = get_categories(array(
                        'orderby' => 'name',
                        'order'   => 'ASC'
                    ));
                    $categories = get_categories(array(
                        'orderby' => 'name',
                        'order'   => 'ASC'
                    )); ?>
                    <ul class="icon-list">
                        <?php
                        $count_categories = count($categories);
                        foreach( $categories as $category ) {
                            $link = get_category_link( $category->term_id );
                            $title = $category->name;
                            echo '<li><a href="'.$link.'">'.$title.'</a></li>';
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<hr class="divider-d">
<footer class="footer bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <p class="copyright font-alt">&copy; 2019&nbsp;<a href="https://neos.lu">NEOS</a>, All Rights Reserved</p>
            </div>
            <div class="col-sm-6">
                <div class="footer-social-links">
                    <a href="https://www.facebook.com/Neosluxembourg/" target="_blank"><i class="fa fa-facebook socialnetwork"></i></a><a href="https://twitter.com/neoslux" target="_blank"><i class="fa fa-twitter socialnetwork"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Global Site Tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-67001397-2"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments)};
    gtag('js', new Date());

    gtag('config', 'UA-67001397-2');
</script>

</div>
<div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
</main>
<?php wp_footer(); ?>
</body>
</html>
