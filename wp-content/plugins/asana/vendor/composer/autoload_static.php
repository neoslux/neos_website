<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitcc914dd4fa3a497591cb956debbcb249
{
    public static $prefixesPsr0 = array (
        'O' => 
        array (
            'OAuth2' => 
            array (
                0 => __DIR__ . '/..' . '/adoy/oauth2/src',
            ),
        ),
        'H' => 
        array (
            'Httpful' => 
            array (
                0 => __DIR__ . '/..' . '/nategood/httpful/src',
            ),
        ),
        'A' => 
        array (
            'Asana\\' => 
            array (
                0 => __DIR__ . '/..' . '/asana/asana/src',
            ),
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixesPsr0 = ComposerStaticInitcc914dd4fa3a497591cb956debbcb249::$prefixesPsr0;

        }, null, ClassLoader::class);
    }
}
