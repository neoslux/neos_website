<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 5/28/2018
 * Time: 1:33 PM
 */




add_action( 'in_admin_header', 'insert_header_admin_tnl' );
function insert_header_admin_tnl()
{
	echo "<asanaScript type=\"text/javascript\">
            var templateUrl = '" . get_site_url() . "';
        </asanaScript>";
}

if (!class_exists('tnl_kpi')) {
	include_once plugin_dir_path( __FILE__ ) . '/class/class-tnl-kpi.php';
}

if (!class_exists('tnl_orders')){
	include_once( get_template_directory() . '/class/class-tnl-kitchenorders.php' );
}

add_action('template_redirect','yoursite_template_redirect');
function yoursite_template_redirect() {
	if ( strpos($_SERVER["REQUEST_URI"],'downloads/data.csv') != false) {
		switch ($_REQUEST["param"])
		{
			case "downloadPayingLuncher":
				header("Content-type: application/x-msdownload",true,200);
				header("Content-Disposition: attachment; filename=" . date("Y") . "-" . date("m") . "-" . date("d") . "-tnl-payingluncher.csv");
				header("Pragma: no-cache");
				header("Expires: 0");
				$result = tnl_kpi::GetNewLuncherWithSubscription(0);
				echo "Week;New paying lunchers" . "\n";
				foreach ($result as $line)
				{
					echo $line->yYear . "-" . $line->yWeek . ";" . $line->sCptluncherpay . "\n";
				}
				exit();
				break;
			case "downloadGeneralNewsletter":
				header("Content-type: application/x-msdownload",true,200);
				header("Content-Disposition: attachment; filename=" . date("Y") . "-" . date("m") . "-" . date("d") . "-tnl-newsletter.csv");
				header("Pragma: no-cache");
				header("Expires: 0");
				$result = tnl_kpi::DownlaodNewsletterSubscriber();
				echo "firstname;lastname;email" . "\n";
				foreach ($result as $line)
				{
					echo $line->firstname . ";" . $line->lastname . ";" . $line->user_email . "\n";
				}
				exit();
				break;
			case "downloadLuncherWithSub":
				header("Content-type: application/x-msdownload",true,200);
				header("Content-Disposition: attachment; filename=" . date("Y") . "-" . date("m") . "-" . date("d") . "-tnl-activeluncher.csv");
				header("Pragma: no-cache");
				header("Expires: 0");
				$result = tnl_kpi::DownlaodActiveLuncher();
				echo "luncher_firstname;luncher_lastname;parent_firstname;parent_lastname;email" . "\n";
				foreach ($result as $line)
				{
					echo $line->luncher_firstname . ";" . $line->luncher_lastname . ";" . $line->parent_firstname .";" . $line->parent_lastname . ";" . $line->email .  "\n";
				}
				exit();
				break;
			default:
				echo $_REQUEST["param"] ;
				break;
		}
	}
}

function load_custom_widgetKPI_styleJS() {
	wp_enqueue_style( 'tnl_kpicss', plugins_url('css/dashboardStyles.css?v=4', __FILE__));
	wp_enqueue_script('jquerykpi', plugins_url('js/jquery-3.2.1.min.js', __FILE__), array('jquery'), '', true);
	wp_enqueue_script('tnl_kpijs', plugins_url('js/dashboard.js?v=3', __FILE__), array('jquery'), '', true);
}
add_action( 'admin_enqueue_scripts', 'load_custom_widgetKPI_styleJS' );

function tnl_add_dashboard_widgetskpi() {

	wp_add_dashboard_widget(
		'tnl_dashboard_new_users',         // Widget slug.
		'KPI : new users', // Title.
		'tnl_dashboard_new_users' // Display function.
	);

	wp_add_dashboard_widget(
		'tnl_dashboard_new_PayingUsers',         // Widget slug.
		'KPI : New users (paying)', // Title.
		'tnl_dashboard_new_PayingUsers' // Display function.
	);

	wp_add_dashboard_widget(
		'tnl_dashboard_new_Luncher',         // Widget slug.
		'KPI : New lunchers', // Title.
		'tnl_dashboard_new_Luncher' // Display function.
	);

	wp_add_dashboard_widget(
		'tnl_dashboard_new_PayingLuncher',         // Widget slug.
		'KPI : New lunchers (paying)', // Title.
		'tnl_dashboard_new_PayingLuncher' // Display function.
	);

	wp_add_dashboard_widget(
		'tnl_dashboard_schoolKpi',         // Widget slug.
		'Top 10 most requested schools', // Title.
		'tnl_dashboard_new_scools_function' // Display function	);/*
	);

	wp_add_dashboard_widget(
		'tnl_dashboard_todayKpi',         // Widget slug.
		'Deliveries today', // Title.
		'tnl_dashboard_today_function' // Display function.
	);

	wp_add_dashboard_widget(
		'tnl_dashboard_CAByDayKpi',         // Widget slug.
		'Delivery revenue', // Title.
		'tnl_dashboard_CAByDayKpi_function' // Display function.
	);

	wp_add_dashboard_widget(
		'tnl_dashboard_BoxByDayKpi',
		'Delivery Box by type',
		'tnl_dashboard_BoxByDayKpi_function'
	);

	wp_add_dashboard_widget(
		'tnl_dashboard_NewsletterAll',
		'Newsletters (stock)',
		'tnl_dashboard_NewsletterAll_function'
	);

	wp_add_dashboard_widget(
		'tnl_dashboard_LuncherWithSubscription',
		'Active lunchers (stock)',
		'tnl_dashboard_LuncherWithSubscription_function'
	);
}
add_action( 'wp_dashboard_setup', 'tnl_add_dashboard_widgetskpi' );

function tnl_dashboard_new_users() {
	$result = tnl_kpi::GetNewUserALL();
	echo "<table class='redTable'>";
	echo "<tr><th>Week</th><th>New users</th></tr>";
	foreach ($result as $line)
	{
		echo "<tr><td>" . $line->yYear . "-" . $line->yWeek . "</td><td>" . $line->sCptUser . "</td></tr>";
	}
	echo "</table>
    <div style=\"width: 100%;text-align: right;display:none;padding-right: 5px;\"> <a href=\"#\" id=\"downloadusers\">download</a></div>";
}

function tnl_dashboard_new_PayingUsers() {
	$result = tnl_kpi::GetNewUserWithSubscription();
	echo "<table class='redTable'>";
	echo "<tr><th>Week</th><th>New paying users</th></tr>";
	foreach ($result as $line)
	{
		echo "<tr><td>" . $line->yYear . "-" . $line->yWeek . "</td><td>" . $line->sCptUservalidSub . "</td></tr>";
	}
	echo "</table>
    <div style=\"width: 100%;text-align: right;display:none;padding-right: 5px;\"> <a href=\"#\" id=\"downloadPayingUsers\">download</a></div>";
}

function tnl_dashboard_new_Luncher() {
	$result = tnl_kpi::GetNewLuncherALL();
	echo "<table class='redTable'>";
	echo "<tr><th>Week</th><th>New lunchers</th></tr>";
	foreach ($result as $line)
	{
		echo "<tr><td>" . $line->yYear . "-" . $line->yWeek . "</td><td>" . $line->sCptluncher . "</td></tr>";
	}
	echo "</table>
    <div style=\"width: 100%;text-align: right;display:none;padding-right: 5px;\"> <a href=\"#\" id=\"downloadLuncher\">download</a></div>";
}

function tnl_dashboard_new_PayingLuncher() {
	$result = tnl_kpi::GetNewLuncherWithSubscription(12);
	echo "<table class='redTable'>";
	echo "<tr><th>Week</th><th>New paying lunchers</th><th>AVG box/week</th></tr>";
	foreach ($result as $line)
	{
		echo "<tr><td>" . $line->yYear . "-" . $line->yWeek . "</td><td>" . $line->sCptluncherpay . "</td><td>" . $line->avglunchweek . "</td></tr>";
	}
	echo "</table>
    <div style=\"width: 100%;text-align: right;padding-right: 5px;\"> <a href=\"#\" id=\"downloadPayingLuncher\">download</a></div>";
}

function tnl_dashboard_today_function() {
	echo tnl_orders::ToDay_DoWork(date('Y-m-d'));
}

function tnl_dashboard_new_scools_function() {
	$result = tnl_kpi::GetNewSchool();
	?>
	<table class='redTable'>
		<tr>
			<th>Name</th>
			<th>Parents</th>
		</tr>

		<?php foreach($result as $line): ?>
			<tr>
				<td><?= $line->Name ?></td>
				<td><?= $line->Cpt ?></td>
			</tr>
		<?php endforeach; ?>

	</table>
	<div style="display:none;width: 100%;text-align: right;padding-right: 5px;"> <a href="#" id="downloadschool">download</a></div>
	<?php
}

function tnl_dashboard_CAByDayKpi_function()
{
	$result = tnl_kpi::GetRevenueByDay(15);
	?>
	<table class='redTable'>
		<tr>
			<th>Date</th>
			<th>Sales revenue</th>
			<th>Average basket</th>
		</tr>

		<?php foreach($result as $line): ?>
			<tr>
				<td><?= $line->dateOfLunch ?></td>
				<td><?= getMoney($line->Revenue) . " " . $line->Money ?></td>
				<td><?= getMoney(($line->Revenue / $line->NbLuncher )) . " " . $line->Money  ?></td>
			</tr>
		<?php endforeach; ?>
	</table>
	<div style="display:none;width: 100%;text-align: right;padding-right: 5px;"> <a href="#" id="downloadrevenue">download</a></div>
	<?php
}

function tnl_dashboard_BoxByDayKpi_function()
{
	$result = tnl_kpi::GetBoxSizeByDay(15);
	?>
	<table class='redTable'>
		<tr>
			<th>Date</th>
			<th>Box S</th>
			<th>Box L</th>
		</tr>

		<?php foreach($result as $line): ?>
			<tr>
				<td><?= $line->dateOfLunch ?></td>
				<td><?= $line->MenuS ?></td>
				<td><?= $line->MenuL ?></td>
			</tr>
		<?php endforeach; ?>

	</table>
	<div style="display:none;width: 100%;text-align: right;padding-right: 5px;"> <a href="#" id="downloadtypeboxperday">download</a></div>
	<?php
}

function tnl_dashboard_NewsletterAll_function()
{
	$result = tnl_kpi::GetBoxSizeByDay(15);
	?>
	<div class='redTable'>
		<?php echo tnl_kpi::GetNumberMembersNewsletter();    ?> users<br/>
		in Newsletter
	</div>
	<div style="width: 100%;text-align: right;padding-right: 5px;"> <a href="#" id="downloadGeneralNewsletter">download</a></div>
	<?php
}

function tnl_dashboard_LuncherWithSubscription_function()
{
	$result = tnl_kpi::GetBoxSizeByDay(15);
	?>
	<div class='redTable'>
		<?php echo tnl_kpi::GetNumberLuncherActif()    ?><br/>
		Active Lunchers
	</div>
	<div style="width: 100%;text-align: right;padding-right: 5px;"> <a href="#" id="downloadLuncherWithSub">download</a></div>
	<?php
}

/*add_action('wp_ajax_ExportCsv', 'ExportCsv');
add_action('wp_ajax_nopriv_ExportCsv', 'ExportCsv');
function ExportCsv() {

}
*/

function getMoney($floating) {
	return sprintf('%0.2f', $floating);
}




//	$me = $client->users->me();
//
//	$workspaces = $me->workspaces;
//	$neosworspaceID =0;
//	foreach($workspaces as $workspace){
//		if($workspace->name === 'Neos Test'){
//			$neosworspaceID = $workspace->id;
//		}
//	}
//	$personalProjectsArray = array_filter($me->workspaces, function($item) { return $item->name === 'Neos Test'; });
//	$personalProjects = array_pop($personalProjectsArray);
//	$projects = $client->projects->findByWorkspace($personalProjects->id, null, array('iterator_type' => false, 'page_size' => null))->data;
//	$project= $projects[0];
//	$tasks = $client->tasks->findByProject($project->id, null, array('iterator_type' => false, 'page_size' => null))->data;
//
//	echo '<pre>';
//
////	var_dump($client->tasks->findAll(array('assignee'=>'me','completed'=>'false'))->pages->client->tasks->client->tasks);
//
//	var_dump($tasks);
//
//	echo '</pre>';