<?php
/*
Plugin Name: Asana
Plugin URI: http://localhost/press/voltaire-gabriel/
Description: Asana Plugin
Version: 0.1
 */

require 'vendor/autoload.php';
require 'class/asanaWrapper.php';

use Asana\Client;

//CONFIG THE GLOBAL VARIABLES
////NEOS TEST
define('ASANA_CLIENT_ID', '702403177890128');
define('ASANA_CLIENT_SECRET', '2441a8c60f924e54aad9ae28552326a9');
define('URL','https://neos.lu/wp-admin');
////VOLTAIRE PROD
//define('ASANA_CLIENT_ID', '713666340644504');
//define('ASANA_CLIENT_SECRET', '004d5d4e77b0c18f8776a35b58e4175d');
//define('URL','https://voltaire.vitalbriefing.com/wp-admin/index.php');
//VOLTAIRE TEST
//define('ASANA_CLIENT_ID', '718382857424157');
//define('ASANA_CLIENT_SECRET', '9f406750f20b55308d798a9b84fa1125');
//define('URL','https://staging.voltaire.vitalbriefing.com/wp-admin/index.php');

/**
 * @method enqueue_scripts
 * @description Load all scripts and style needed by the plugins
 */
add_action('admin_enqueue_scripts', 'asana_load_js');
function asana_load_js(){
    //Load and declare the plugins stylesheet
	wp_register_style('css', plugins_url('asana/asanaCss/css.css'));
    wp_enqueue_style('css');

    //Load and declare the plugins script
    wp_register_script('asanaScript', plugins_url('asana/asanaScript/asanaScript.js'), array('jquery'),false,true);
    wp_enqueue_script('asanaScript');

    //Load and declare the libraries used inside the plugins
	wp_register_style('font-awesome.min.css', plugins_url( 'asana/lib/font-awesome/css/font-awesome.min.css' ));
	wp_enqueue_style('font-awesome.min.css' );
	//As jQuery is included in WordPress, no need to declare it
	wp_enqueue_script( 'jquery' );

}

/**
 * @method add_dashboard_widget
 * @description create the dashboard widget
 */
function add_dashboard_widgetskpi() {

	wp_add_dashboard_widget(
		'show-asana-tasks',     // Widget slug.
		'VOLTAIRE-ASANA | Your upcoming tasks from Asana', // Title.
		'dashboard_tasks_list' // Display function.
	);
}

//TEMPLATES
function Task(){
    return "<div class='taskrow'>
                <div class='completeTask' data-id='{{ID}}' data-token='{{TOKEN}}' data-assignee='{{ASSIGNEE}}' >
                    <span class='icon-container'>
                        <i class='fa fa-check-circle-o fa-2x checkicon' id='checkIcon'></i>
                    </span>
                    <span class='tasktitle'>{{NAME}}</span>
                    <span class='due-date'>{{DUE}}</span>
                </div>
            </div>";
}

function HeaderAsana(){
    return "<div class='asanaHeader'>
                <div class='asanaHeaderContainer'>
                    <a href='https://app.asana.com' target='_blank'>
                        <img class='asanalogo' src='" . plugins_url('asana/asanaImg/Asana_logo_new.png') . "'/>
                    </a>
                </div>
            </div>";
}

function FooterAsana(){
    return "";
}

function CodeObtained(){
    return "<input type='hidden' id='codeObtained' data-url='".get_bloginfo('wpurl')."/wp-admin/index.php'>";
}

function MainContainer($token){
    return "<main class='asanaMainContainer' id='asanaMainContainer' data-url='" . get_bloginfo('wpurl') . "' data-token = '" . $token . "'></main>";
}

function newTask($id,$name,$token){

    /*if(strtotime($task->due_on)){
        $due = date('j M',strtotime($task->due_on));
    }else{
        $due = '';
    }*/

    $assignee = $id;

    $aimedElements = array('{{ID}}','{{TOKEN}}','{{NAME}}','{{DUE}}','{{ASSIGNEE}}');
    $replacedBy = array($id, $token, $name, '', $assignee);
    return str_replace($aimedElements, $replacedBy, Task());
}

function Connect(){
    return Client::oauth(array(
        'client_id' => ASANA_CLIENT_ID,
        'client_secret' => ASANA_CLIENT_SECRET,
        'redirect_uri' => URL,
//        'redirect_uri' => 'https://neos.lu/wp-admin',
    ));
}

function getToken($client, $code){
    return $client->dispatcher->fetchToken($code);
}

function getAuthUrl($client){
    return $client->dispatcher->authorizationUrl();
}

function Wrap($token){
    return new asanaWrapper(
        ['accessToken' => $token]
    );
}

function getAsanaWorkspaces($wrapper){
    return json_decode($wrapper->getWorkspaces(), false, 512, JSON_BIGINT_AS_STRING)->data;
}

function getAsanaTasks($wrapper, $workspace){
    return json_decode($wrapper->getWorkspaceTasks($workspace->id, 'me', array('completed_since' => 'now')), false, 512, JSON_BIGINT_AS_STRING)->data;
}

function dashboard_tasks_list()
{

    // get and test  asana key
    $userId   = get_current_user_id();
    $isAuth = get_user_meta( $userId, 'authentified', true );
    $client = Connect();

    if($isAuth && !isset($_GET['code'])){
        echo '<input type="hidden" id="authentified" data-link="' . getAuthUrl($client) . '">' ;

    }else if(!$isAuth){
        echo "<a href='" . getAuthUrl($client) . "' target='_self' >Authorize Asana</a>";
    }

    $asanacache = get_user_meta($userId,'AsanaCache',true);
    $asanadatecache = get_user_meta($userId,'AsanaDateCache',true);
    $datefrom = new DateTime($asanadatecache);
    $dateto = new DateTime('now');
    $diff = $datefrom->diff($dateto);

    error_log($diff->i);

    if(!isset($asanacache)&&($asanacache!="")){
        //TODO
    }
    $code = isset($_GET['code']) ? $_GET['code'] : null;
    if(!$isAuth){
        add_user_meta($userId,'authentified' , true);
    }

    if (!empty($code)) {
        $token = getToken($client,$code);
        $asana = Wrap($token);
        update_user_meta($userId,'asana',$asana);
        update_user_meta($userId,'asanaToken',$token);
        delete_user_meta($userId,'AsanaCache');
        // get and test  asana key
        $workspaces = getAsanaWorkspaces($asana);

        $tasksLine = "";
        foreach ($workspaces as $workspace) {
            $tasks = getAsanaTasks($asana, $workspace);
            foreach ($tasks as $task) {
                $tasksLine .= newTask($task->id, $task->name,$token);
            }
        }
        $Chainehtml = HeaderAsana() .  CodeObtained() . MainContainer($token) . $tasksLine . FooterAsana();
        update_user_meta($userId, "AsanaCache", $Chainehtml);
        update_user_meta($userId, "AsanaDateCache", date("Y-m-d H:i:s"));
        echo $Chainehtml;
    }
}

/* Service schelduler : Jérôme */

/* Add Script Asana for timer in all admin page */
add_action( 'admin_enqueue_scripts', 'asana_enqueue_scripts' );
function asana_enqueue_scripts() {
    $userid = get_current_user();
    if(isset($userid)){
        //Load and declare the plugins script
        wp_register_script('asanaScript2', plugins_url('asana/asanaScript/asanaTics.js'), array('jquery'),false,true);
        wp_enqueue_script('asanaScript2');
    }
}

/* Colled methode all 1 minutes by a script client to update the caching of ASANA per user*/
add_action('wp_ajax_TicFomUsert', 'TicFomUsert');
add_action('wp_ajax_nopriv_TicFomUsert', 'TicFomUsert');
function TicFomUsert()
{
    try{
        $userId   = get_current_user_id();
        if($userId<=0){ error_log("Tic Asana Plugin: violation tentative, user not autorized \n"); }
        else{
            error_log("Enter in a Tic refresh");
            $asana = get_user_meta($userId,"asana",true);
            $token = get_user_meta($userId,"asanaToken",true);
            if(isset($asana)){
                $workspaces = getAsanaWorkspaces($asana);
                $tasksLine = "";
                foreach ($workspaces as $workspace) {
                    $tasks = getAsanaTasks($asana, $workspace);
                    foreach ($tasks as $task) {
                        $tasksLine .= newTask($task->id, $task->name,$token);
                    }
                }
                $Chainehtml = HeaderAsana() .  CodeObtained() . MainContainer($token) . $tasksLine . FooterAsana();
                // Update of caching
                get_user_meta($userId,"AsanaCache", $Chainehtml);
                update_user_meta($userId, "AsanaDateCache", date("Y-m-d H:i:s"));
            }
        }
    }
    catch (Exception $e) {
        error_log("Error Tic Asana Plugin: " . $e->getMessage() + "\n");
    }
}

/*  */

    add_action('wp_ajax_completedTask', 'completedTask');
    add_action('wp_ajax_nopriv_completedTask', 'completedTask');
    function completedTask()
    {
        $userId   = get_current_user_id();
        $asana = get_user_meta( $userId, 'asana', true );
        $taskId = $_POST['taskId'];

        $asana->updateTask($taskId, array('completed' => true));

        wp_send_json(1);
    }

    add_action('wp_ajax_checkAsana', 'checkAsana');
    add_action('wp_ajax_nopriv_checkAsana', 'checkAsana');
    function checkAsana()
    {
        $tokenAsana = $_POST['tokenAsana'];
        $userId = $_POST['userId'];
        try {
            $asana = new asanaWrapper([
                'accessToken' => $tokenAsana
            ]);
            wp_send_json(1);
        } catch (Exception $e) {
            wp_send_json(0);
        }


    }

add_action('wp_ajax_refreshTask', 'refreshTask');
add_action('wp_ajax_nopriv_refreshTask', 'refreshTask');
function refreshTask(){
    $userId   = get_current_user_id();
    $asana = get_user_meta( $userId, 'asana', true );

    $workspaces = getAsanaWorkspaces($asana);

    foreach ($workspaces as $workspace) {
        $tasks = getAsanaTasks($asana,$workspace);
        foreach ($tasks as $task) {
            $tempString = $asana->getTask($task->id);
            $completetask = json_decode($tempString, false, 512, JSON_BIGINT_AS_STRING);
            $tasklist[] = $completetask;
        }
    }

    wp_send_json($tasklist);
}


add_action( 'wp_dashboard_setup', 'add_dashboard_widgetskpi' );

?>