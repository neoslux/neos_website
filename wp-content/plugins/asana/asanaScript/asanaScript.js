/*global jQuery, window, ajaxurl, console*/

jQuery(function ($) {

    'use strict';

    var auth;
    auth = $('#authentified');

    function stopWheel(e) {
        if (!e) {
            e = window.event;
        } /* IE7, IE8, Chrome, Safari */
        if (e.preventDefault) {
            e.preventDefault();
        } /* Chrome, Safari, Firefox */
        e.returnValue = false; /* IE7, IE8 */
    }

    function scrollToTop(){
        window.scrollTo(0, 0);
    }

    if (auth.length !== 0) {
        window.location.href = auth.attr('data-link');
    }

    $("#btnSubmit").click(function(event){
        var tokenAsana;
        var userId;

        event.preventDefault();

        tokenAsana = $("#asana_key").val();
        userId = $("#userId").val();

        $.ajax({
            type: "POST",
            url: ajaxurl,
            dataType: "JSON",
            data: {
                "action": "checkAsana",
                "tokenAsana": tokenAsana,
                "userId": userId
            }
        }).done(function(response) {
            if(response){
                location.reload();
            }else{
                console.error('Invalid Token');
            }
        });
    });

    $('.tasktitle').on('click', function(){
        var assignee;
        var task;

        assignee = $(this).parent().attr('data-assignee');
        task = $(this).parent().attr('data-id');

        window.open('https://app.asana.com/0/' + assignee + '/' + task);
    });

    function validateModal(element){
        console.log(element);
        scrollToTop();

        //lock mouse scroll
        document.onmousewheel = function(){
            stopWheel();
        }; /* IE7, IE8 */
        if(document.addEventListener){ /* Chrome, Safari, Firefox */
            document.addEventListener('DOMMouseScroll', stopWheel, false);
        }

        var that = $(element).parent().parent();

        var title = that.find('.tasktitle').html();
        var taskId = that.attr('data-id');
        var modalexist = $('#modal').length;
        var popup = '</div>' +
            '<div class="modalAsana" id="modal">' +
            '<p class="modal_p blue">MARK THIS TASK AS COMPLETED ?</p>' +
            '<p class="task-modal">' + title + '</p>' +
            '<div class="right">' +
            '<button id="no"  class="asana-button">No</button>' +
            '<button id="yes" class="asana-button">Yes</button>' +
            '</div>' +
            '</div>' +
            '<div class="screen">&nbsp;</div>';

        if (modalexist===0){
            $("body").append(popup);
            $('#yes').on('click',function(){
                $("body").css('cursor', 'progress');
                $('#modal').remove();
                $('.screen').remove();
                that.addClass('taskrow-current');
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: {
                        'action': 'completedTask',
                        'taskId': taskId
                    }
                }).done(function() {
                    that.parent().remove();
                    $("body").css('cursor', 'auto');
                    //Re-enabling the Wheel
                    document.onmousewheel = null;  /* IE7, IE8 */
                    if(document.addEventListener){ /* Chrome, Safari, Firefox */
                        document.removeEventListener('DOMMouseScroll', stopWheel, false);
                    }
                });
            });
            $('#no').on('click',function(){
                $('#modal').remove();
                $('.screen').remove();
                //Re-enabling the Wheel
                document.onmousewheel = null;  /* IE7, IE8 */
                if(document.addEventListener){ /* Chrome, Safari, Firefox */
                    document.removeEventListener('DOMMouseScroll', stopWheel, false);
                }
            });
        }
    }

    $('.checkicon').on('click', function(){
        validateModal(this);
    });

    function goToTask(element){
        var assignee; var taskId;
        assignee = $(element).parent().attr('data-assignee');
        taskId = $(element).parent().attr('data-id');
        window.open('https://app.asana.com/0/' + assignee + '/' + taskId);
    }

    $('.tasktitle').on('click', function(){
       goToTask(this);
    });

    if($('#codeObtained').length > 0){
        var stateObj = { name: "voltaire" };
        var url = $('#codeObtained').attr('data-url');
        history.pushState(stateObj, "Voltaire", url);
        localStorage.setItem('asana-code', true);
    }

    function toggleLoader(){
        $('.taskrow').addClass('disappear');
        setTimeout(function(){
            $('.taskrow').remove();
            var siteurl = $('#asanaMainContainer').attr('data-url');
            $('#asanaMainContainer').addClass('center');
            // $('#asanaMainContainer').html('<img src="' + siteurl + '/wp-content/plugins/asana/asanaImg/loader.gif" class="appear" style="width:10%;opacity:.2">');
        }, 1000);
    }

    setInterval(function(){

        $.ajax({
            beforeSend: toggleLoader,
            type: 'POST',
            url: ajaxurl,
            data: {
                'action': 'refreshTask'
            }
        }).done(function(response){
            var token = $('#asanaMainContainer').attr('data-token');
            $('#asanaMainContainer img').removeClass('appear').addClass('disappear');
            setTimeout(function(){
                $('#asanaMainContainer img').remove();
                $('#asanaMainContainer').removeClass('center');
                var taskTemplate;
                    $.each(response, function(key, value){
                        var datef;
                        if(value.data.due_on !== null){

                            var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                            var DatePre = new Date(value.data.due_on);
                            datef = DatePre.getDate() + " " + monthNames[DatePre.getMonth()];
                        }else{
                            datef = "";
                        }

                        var total = (key + 1) *200;
                        setTimeout(function(){
                            taskTemplate =
                                "<div class='taskrow'>" +
                                "<div class='completeTask' data-id='" + value.data.id + "' data-token='" + token + "' data-assignee='" + value.data.assignee.id + "' class='appear'>" +
                                "<span class='icon-container appear'>" +
                                "<i class='fa fa-check-circle-o fa-2x checkicon appear' id='checkIcon'></i>" +
                                "</span> " +
                                "<span class='tasktitle appear'>" + value.data.name + "</span> " +
                                "<span class='due-date appear'>" + datef +"</span> " +
                                "</div> " +
                                "</div>";

                            $('#asanaMainContainer').append(taskTemplate);
                            $('.completeTask[data-id="' + value.data.id + '"] .checkicon').on('click', function(){
                                validateModal(this);
                            });

                            $('.completeTask[data-id="' + value.data.id + '"] .tasktitle').on('click', function(){
                                goToTask(this);
                            });
                        },total);
            },500);
            });
        });
    },999999999);

});
